
// Import thư viện Express Js
const express = require("express");

const { drinkClassList, drinkObj } = require("./drink.js");
// Khởi tạo 1 app express
const app = express();

// Khai báo 1 cổng 
const port = 8000;

// Cấu hình để API đọc được body JSON
app.use(express.json());

// Cấu hình để API đọc được body có ký tự tiếng Việt
app.use(express.urlencoded({
    extended: true
}));

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    response.status(200).json({
        message: "Drinks API"
    })
});

// Khởi tạo GET API trả về drink class
app.get("/drink-class", (req, res) => {
    let code = req.query.code; // lấy giá trị của key code trong query parameters gửi lên
    if (code) {
        res.status(200).json({
            result: drinkClassList.filter((item) => item.checkDrinkByCode(code))
        })
    }
    else {
        res.status(200).json({
            result: drinkClassList
        })
    }
});

// Khởi tạo GET API trả về drink class
app.get("/drink-class/:drinkId", (req, res) => {
    let drinkId = req.params.drinkId;
    // Do drinkId nhận được từ params là string nên cần chuyển thành kiểu integer
    drinkId = parseInt(drinkId);
    res.status(200).json({
        result: drinkClassList.filter((item) => item.checkDrinkById(drinkId))
    })

});
// Khởi tạo GET API trả về drink Obj
app.get("/drink-object", (req, res) => {
    let code = req.query.code; // lấy giá trị của key code trong query parameters gửi lên
    if (code) {
        // chuyển code thành chuỗi chữ in hoa
        code = code.toUpperCase();
        res.status(200).json({
            result: drinkObj.filter((item) => item.code.includes(code))
        })
    }
    else {
        res.status(200).json({
            result: drinkObj
        })
    }
});

// Khởi tạo GET API  trả về drink Obj
app.get("/drink-object/:drinkId", (req, res) => {
    let drinkId = req.params.drinkId;
    // Do drinkId nhận được từ params là string nên cần chuyển thành kiểu integer
    drinkId = parseInt(drinkId);
    res.status(200).json({
        result: drinkObj.filter((item) => item.id === drinkId)
    })
});
// run app on declared port
app.listen(port, () => {
    console.log(`App running on port ${port}`);
})





