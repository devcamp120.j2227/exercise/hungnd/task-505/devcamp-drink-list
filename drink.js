// Khai báo class drink
class Drink {
    constructor(id, code, name, price, dateCreated, dateUpdated) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.price = price;
        this.dateCreated = dateCreated;
        this.dateUpdated = dateUpdated;
    }
    // Hàm kiểm tra đồ uống có chứa ID được truyền vào hay không
    checkDrinkById(paramId) {
        return this.id === paramId
    }
    // Hàm kiểm tra đồ uống có code chứa mã được truyền vào hay không
    checkDrinkByCode(paramCode) {
        return this.code.includes(paramCode.toUpperCase());
    }
};
// khởi tạo đối tượng Drink class
let drink1 = new Drink(1, "TRATAC", "Trà tắc", 10000, "14/5/2021", "14/5/2021");
let drink2 = new Drink(2, "COCA", "Cocacola", 15000, "14/5/2021", "14/5/2021");
let drink3 = new Drink(3, "PEPSI", "Pepsi", 15000, "14/5/2021", "14/5/2021");
// Khai báo mảng chứa toàn bộ thông tin đồ uống;

let drinkClassList = [drink1, drink2, drink3];
// Khởi tạo đối tượng Object chứa các đối tượng drink
let drinkObj = [
    {id: 1,code: "TRATAC",name: "Trà tắc",price: 10000,dateCreated: "14/5/2021",dateUpdated: "14/5/2021"},
    {id: 2,code: "COCA",name: "Cocacola",price: 15000,dateCreated: "14/5/2021",dateUpdated: "14/5/2021"},
    {id: 3,code: "PEPSI",name: "Pepsi",price: 15000,dateCreated: "14/5/2021",dateUpdated: "14/5/2021"}
];
module.exports = {
    drinkClassList,
    drinkObj
}

